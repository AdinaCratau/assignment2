
const FIRST_NAME = "Adina-Camelia";
const LAST_NAME = "Cratau";
const GRUPA = "1090";

/**
 * Make the implementation here
 */

function initCaching() {
    var sections = {

    }

    function pageAccessCounter(pageName='home') {
        var found = 0;

        pageName = pageName.toLowerCase();
        
        for(var sec in sections) {
            if(sec == pageName) 
            {
                found = 1;
                break;
            }
        }

        if(found == 1) {
            sections[pageName]++;
        }
        else
        {
            sections[pageName] = 1;
        }
    }
    
    function getCache(cache) {
        return sections
    }

    var cache = {
        pageAccessCounter: pageAccessCounter,
        getCache: getCache
    }

    return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

